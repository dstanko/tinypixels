#include "tinypixels.h"

#include "renderer_opengl.h"
#include "renderer_vulkan.h"

#include <GLFW/glfw3.h>

#include <thread>
#include <chrono>
#include <algorithm>
#include <atomic>

namespace tpx
{
struct TinyPixelsContext
{
    struct GLFWwindow* glfw_window;
    const char* title;
    void* debug_user_data;
    DebugCallback* debug_callback;
    RenderType render_type;
    bool inited;
} self;

void next_frame()
{
    if (self.render_type == RenderType::OPENGL)
    {
        opengl_next_frame();
        draw_clear();
    }

    if (time.update_period_target != 0)
    {
        int64_t next_target = (time.frame_no + 1) * time.update_period_target;
        int64_t now;
        time_now(&now);
        int64_t delta = next_target - now;
        if (delta > 0)
            std::this_thread::sleep_for(std::chrono::duration(std::chrono::microseconds(delta)));
    }

    time.frame_no++;
    int64_t prev_timestamp = time.frame_timestamp;
    time_now(&time.frame_timestamp);
    time.frame_delta = time.frame_timestamp - prev_timestamp;

    input_update(&input);
}

ErrCode open_window(const char* title, int width, int height, bool fullscreen, bool vsync, int max_fps, RenderType render_type)
{
    self = TinyPixelsContext();
    self.title = title;
    self.render_type = RenderType::ERROR;
    time.update_period_target = max_fps != NO_MAX_FPS ? (2'000'000 + max_fps) / (2 * max_fps) : 0;

    ErrCode render_error = ErrCode::ERROR_UNKNOWN;
    if (render_type == RenderType::AUTO)
    {
        render_error = init_vulkan(&self.glfw_window, title, width, height, fullscreen, vsync);
        if (render_error == ErrCode::SUCCESS)
        {
            self.render_type = RenderType::VULKAN;
        }
        else
        {
            destroy_vulkan(&self.glfw_window);

            render_error = opengl_init(&self.glfw_window, title, width, height, fullscreen, vsync);
            if (render_error == ErrCode::SUCCESS)
            {
                self.render_type = RenderType::OPENGL;
            }
            else
            {
                opengl_destroy();
            }
        }
    }
    else if (render_type == RenderType::VULKAN)
    {
        render_error = init_vulkan(&self.glfw_window, title, width, height, fullscreen, vsync);
        if (render_error == ErrCode::SUCCESS)
        {
            self.render_type = RenderType::VULKAN;
        }
        else
        {
            destroy_vulkan(&self.glfw_window);
        }
    }
    else if (render_type == RenderType::OPENGL)
    {
        render_error = opengl_init(&self.glfw_window, title, width, height, fullscreen, vsync);
        if (render_error == ErrCode::SUCCESS)
        {
            self.render_type = RenderType::OPENGL;
        }
        else
        {
            opengl_destroy();
        }
    }
    else
    {
        render_error = ErrCode::ERROR_NOT_SUPPORTED;
    }

    if (render_error != ErrCode::SUCCESS)
    {
        return render_error;
    }

    camera = Camera{0, 0, width, height, 0x00};
    self.inited = true;
    return ErrCode::SUCCESS;
}

ErrCode input_update(Input* input)
{
    glfwPollEvents();
    time_now(&input->timestamp);
    if (input != nullptr)
    {
        bool new_state[348] = {};

        for (int i = 0; i < 348; ++i)
            new_state[i] = glfwGetKey(self.glfw_window, i) == GLFW_PRESS;

        for (int i = 0; i < 348; ++i)
            input->is_released[i] = input->is_down[i] && !new_state[i];
        for (int i = 0; i < 348; ++i)
            input->is_pressed[i] = !input->is_down[i] && new_state[i];
        for (int i = 0; i < 348; ++i)
            input->is_down[i] = new_state[i];
        tpx::window_close_requested(&input->is_close_window);
    }
    return ErrCode::SUCCESS;
}

ErrCode start_debug(DebugCallback* callback, void* user_data)
{
    self.debug_callback = callback;
    self.debug_user_data = user_data;

    if (self.render_type == RenderType::VULKAN)
    {
        start_debug_vulkan(callback, user_data);
    }
    else if (self.render_type == RenderType::OPENGL)
    {
        opengl_start_debug(callback, user_data);
    }
    return ErrCode::SUCCESS;
}

ErrCode send_debug(const char* msg)
{
    ErrCode sts = ErrCode::ERROR_UNKNOWN;

    if (self.debug_callback != nullptr)
    {
        self.debug_callback(msg, self.debug_user_data);
        sts = ErrCode::SUCCESS;
    }
    else
    {
        sts = ErrCode::ERROR_INVALID_OPERATION;
    }
    return sts;
}

ErrCode close_window()
{
    if (self.render_type == RenderType::VULKAN)
    {
        return destroy_vulkan(&self.glfw_window);
    }
    else if (self.render_type == RenderType::OPENGL)
    {
        return opengl_destroy();
    }
    else
    {
        return ErrCode::ERROR_UNKNOWN;
    }
}

ErrCode window_close_requested(bool* close_requested)
{
    if (close_requested != nullptr)
        *close_requested = glfwWindowShouldClose(self.glfw_window);
    return ErrCode::SUCCESS;
}

void camera_move(int x, int y)
{
    camera.x = x;
    camera.y = y;
    if (self.render_type == RenderType::OPENGL)
    {
        opengl_update_camera(x, y, camera.width, camera.height);
    }
}

ErrCode time_now(int64_t* total_time)
{
    if (total_time != nullptr)
    {
        *total_time = (uint64_t)(glfwGetTime() * 1'000'000); // should be okey for atleast some thousands of years
        return ErrCode::SUCCESS;
    }
    return ErrCode::ERROR_INVALID_ARGUMENT;
}

std::atomic<bool> run_gui = false;

ErrCode start_gui_loop()
{
    run_gui = true;
    while (run_gui)
    {
        glfwWaitEvents();
    }
    return ErrCode::SUCCESS;
}

ErrCode stop_gui_loop()
{
    run_gui = false;
    glfwPostEmptyEvent();
    return ErrCode::SUCCESS;
}

} // namespace tpx

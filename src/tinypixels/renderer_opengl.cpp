#include "renderer_opengl.h"

#include <iostream>

#include "shaders.h"
#include "tinypixels.h"

#include <doctest.h>
#include <gllpp.hpp>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <cstddef>

namespace
{
struct ShaderProgram
{
    GLuint program;
    GLuint vertex_shader;
    GLuint fragment_shader;
    GLuint vbo;
    GLuint vbo_instancing;
    GLuint vao;
};

struct LinePackage
{
    GLfloat start[2];
    uint8_t start_color[4];
    GLfloat stop[2];
    uint8_t stop_color[4];
};

struct RendererOpengl
{
    GLFWwindow* glfw_window;
    ShaderProgram shader_primitive;
    ShaderProgram shader_sprite;

    glm::mat4 camera;
} self;

void opengl_create_shader(ShaderProgram* program, const char* vertex_shader, const char* fragment_shader)
{
    program->vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(program->vertex_shader, 1, &vertex_shader, NULL);
    glCompileShader(program->vertex_shader);

#ifndef NDEBUG
    {
        int success;
        char infoLog[512];
        glGetShaderiv(program->vertex_shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(program->vertex_shader, 512, NULL, infoLog);
            REQUIRE(success);
        }
    }
#endif // DEBUG

    program->fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(program->fragment_shader, 1, &fragment_shader, NULL);
    glCompileShader(program->fragment_shader);

#ifndef NDEBUG
    {
        int success;
        char infoLog[512];
        glGetShaderiv(program->fragment_shader, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(program->fragment_shader, 512, NULL, infoLog);
            REQUIRE(success);
        }
    }
#endif // DEBUG

    program->program = glCreateProgram();
    glAttachShader(program->program, program->vertex_shader);
    glAttachShader(program->program, program->fragment_shader);
    glLinkProgram(program->program);

#ifndef NDEBUG
    {
        int success;
        char infoLog[512];
        glGetProgramiv(program->program, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(program->fragment_shader, 512, NULL, infoLog);
            REQUIRE(success);
        }
    }
#endif // DEBUG

    //todo(markusl): investigate failure
    //glBindVertexArray(0);
    //glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK(glGetError() == GL_NO_ERROR);
}

void opengl_destroy_shader(ShaderProgram* program)
{
    REQUIRE(program != nullptr);
    glDeleteProgram(program->program);
    glDeleteShader(program->vertex_shader);
    glDeleteShader(program->fragment_shader);
    CHECK(glGetError() == GL_NO_ERROR);
}
} // namespace

namespace tpx
{
void opengl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
void opengl_draw_clear();
void opengl_draw_line(int start_x, int start_y, int stop_x, int stop_y, Color start_color, Color stop_color);
ErrCode opengl_sprite_resource_load(SpriteResource* sprite_resource);
ErrCode opengl_sprite_resource_unload(SpriteResource* sprite_resource);
void opengl_draw_sprite(const SpriteResource* spr, int x, int y, int subimage);
void opengl_draw_sprites(const SpriteResource* img, const SpriteInstance* pkg, int count);
ErrCode opengl_aquire_context();
ErrCode opengl_release_context();

ErrCode opengl_init(GLFWwindow** glfw_window, const char* title, int width, int height, bool fullscreen, bool vsync)
{
    if (!glfwInit())
    {
        return ErrCode::ERROR_NOT_SUPPORTED;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    *glfw_window = glfwCreateWindow(width, height, title, nullptr, nullptr);
    self.glfw_window = *glfw_window;

    if (self.glfw_window == nullptr)
    {
        glfwTerminate();
        return ErrCode::ERROR_UNKNOWN;
    }
    glfwMakeContextCurrent(self.glfw_window);
    glfwSetInputMode(self.glfw_window, GLFW_STICKY_KEYS, GLFW_TRUE);
    glInit();
    if (glGetError() != GL_NO_ERROR)
    {
        glfwTerminate();
        ErrCode::ERROR_NOT_SUPPORTED;
    }

    opengl_start_debug(nullptr, nullptr);

    // Setup OpenGL
    glViewport(0, 0, width, height);
    glfwSwapInterval(vsync ? 1 : 0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    REQUIRE(glGetError() == GL_NO_ERROR);

    // Setup camera
    self.camera = glm::ortho(0.f, (float)width, (float)height, 0.f);

    // Setup primitive shader
    opengl_create_shader(&self.shader_primitive, shader_vertex_primitive_src, shader_fragment_primitive_src);
    glGenVertexArrays(1, &self.shader_primitive.vao);
    glBindVertexArray(self.shader_primitive.vao);
    glGenBuffers(1, &self.shader_primitive.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, self.shader_primitive.vbo);
    constexpr GLsizei STRIDE = offsetof(LinePackage, stop);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, STRIDE, (void*)0);
    glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, STRIDE, (void*)offsetof(LinePackage, start_color));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);

    REQUIRE(glGetError() == GL_NO_ERROR);

    // Setup sprite shader
    opengl_create_shader(&self.shader_sprite, shader_vertex_sprite_src, shader_fragment_sprite_src);
    glGenVertexArrays(1, &self.shader_sprite.vao);
    glBindVertexArray(self.shader_sprite.vao);
    glGenBuffers(1, &self.shader_sprite.vbo);
    glGenBuffers(1, &self.shader_sprite.vbo_instancing);
    glBindBuffer(GL_ARRAY_BUFFER, self.shader_sprite.vbo);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)0);                   // quad_pos
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)(sizeof(float) * 2)); // tex_coord
    glBindBuffer(GL_ARRAY_BUFFER, self.shader_sprite.vbo_instancing);
    glVertexAttribPointer(2, 2, GL_INT, GL_FALSE, sizeof(int) * 3, (void*)0);        // pos
    glVertexAttribIPointer(3, 1, GL_INT, sizeof(int) * 3, (void*)(sizeof(int) * 2)); // sub_image
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    glVertexAttribDivisor(2, 1);
    glVertexAttribDivisor(3, 1);
    glBindVertexArray(0);

    REQUIRE(glGetError() == GL_NO_ERROR);

    opengl_update_camera(0, 0, width, height);

    // Bind functions
    draw_clear = &opengl_draw_clear;
    draw_line = &opengl_draw_line;

    resource_load_sprite = &opengl_sprite_resource_load;
    resource_unload_sprite = &opengl_sprite_resource_unload;
    draw_sprite = &opengl_draw_sprite;
    draw_sprites = &opengl_draw_sprites;
    aquire_context = &opengl_aquire_context;
    release_context = &opengl_release_context;

    draw_clear();

    return ErrCode::SUCCESS;
}

ErrCode opengl_destroy()
{
    opengl_destroy_shader(&self.shader_primitive);

    CHECK(glGetError() == GL_NO_ERROR);

    if (self.glfw_window != nullptr)
        glfwDestroyWindow(self.glfw_window);
    self.glfw_window = nullptr;

    glfwTerminate();

    return ErrCode::SUCCESS;
}

ErrCode opengl_next_frame()
{
    glfwSwapBuffers(self.glfw_window);
    return ErrCode::SUCCESS;
}

ErrCode opengl_update_camera(int x, int y, int width, int height)
{
    CHECK(width > 0);
    CHECK(height > 0);

    self.camera = glm::ortho((float)x, (float)(x + width), (float)(y + height), (float)y);

    glUseProgram(self.shader_primitive.program);
    GLuint primitive_camera_loc = glGetUniformLocation(self.shader_primitive.program, "camera");
    glUniformMatrix4fv(primitive_camera_loc, 1, GL_FALSE, &self.camera[0][0]);

    glUseProgram(self.shader_sprite.program);
    GLuint sprite_camera_loc = glGetUniformLocation(self.shader_sprite.program, "camera");
    glUniformMatrix4fv(sprite_camera_loc, 1, GL_FALSE, &self.camera[0][0]);

    REQUIRE(glGetError() == GL_NO_ERROR);

    return ErrCode::SUCCESS;
}

ErrCode opengl_start_debug(DebugCallback callback, void* user_data)
{
    ErrCode sts = ErrCode::ERROR_NOT_SUPPORTED;
    if (glDebugMessageCallback)
    {
        glDebugMessageCallback(&opengl_debug_callback, user_data);
        sts = ErrCode::SUCCESS;
    }
    CHECK(glGetError() == GL_NO_ERROR);
    return sts;
}

void opengl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    std::cout << message << std::endl;
    send_debug(message);
}

void opengl_draw_clear()
{
    glClearColor(camera.clear_color[0] / 255.0f, camera.clear_color[1] / 255.0f, camera.clear_color[2] / 255.0f, camera.clear_color[3] / 255.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    REQUIRE(glGetError() == GL_NO_ERROR);
}

void opengl_draw_line(int start_x, int start_y, int stop_x, int stop_y, Color start_color, Color stop_color)
{
    LinePackage data = {{(float)start_x, (float)start_y},
                        {start_color[0], start_color[1], start_color[2], start_color[3]},
                        {(float)stop_x, (float)stop_y},
                        {stop_color[0], stop_color[1], stop_color[2], stop_color[3]}};

    glUseProgram(self.shader_primitive.program);
    glBindVertexArray(self.shader_primitive.vao);
    glBindBuffer(GL_ARRAY_BUFFER, self.shader_primitive.vbo);

    glBufferData(GL_ARRAY_BUFFER, sizeof(data), &data, GL_STREAM_DRAW);

    glDrawArrays(GL_LINES, 0, 2);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK(glGetError() == GL_NO_ERROR);
}

ErrCode opengl_sprite_resource_load(SpriteResource* sprite_resource)
{
    REQUIRE(sprite_resource != nullptr);
    glGenTextures(1, (GLuint*)&sprite_resource->data_gl);
    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)sprite_resource->data_gl);
    glTexImage3D(GL_TEXTURE_2D_ARRAY,
                 0,
                 GL_RGBA8,
                 sprite_resource->width,
                 sprite_resource->height,
                 sprite_resource->no_subimages,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 0);
    glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
                    0,
                    0,
                    0,
                    0,
                    sprite_resource->width,
                    sprite_resource->height,
                    sprite_resource->no_subimages,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    sprite_resource->ram_data);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //TODO: investigate with rotation
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    CHECK(glGetError() == GL_NO_ERROR);

    return ErrCode::SUCCESS;
}

ErrCode opengl_sprite_resource_unload(SpriteResource* sprite_resource)
{
    REQUIRE(sprite_resource != nullptr);
    glDeleteTextures(1, (GLuint*)&sprite_resource->data_gl);

    CHECK(glGetError() == GL_NO_ERROR);

    return ErrCode::SUCCESS;
}

void opengl_draw_sprite(const SpriteResource* spr, int x, int y, int subimage)
{
    SpriteInstance insta;
    insta.x = x;
    insta.y = y;
    insta.subimage = subimage;
    opengl_draw_sprites(spr, &insta, 1);
}

void opengl_draw_sprites(const SpriteResource* spr, const SpriteInstance* instances, int count)
{
    float vertices[] = {
        // clang-format off
        (float)0,          (float)0,           0.f, 0.f, // top left
        (float)spr->width, (float)0,           1.f, 0.f, // top right
        (float)0,          (float)spr->height, 0.f, 1.f, // bottom left
        (float)spr->width, (float)spr->height, 1.f, 1.f  // bottom right
        // clang-format on
    };
    glUseProgram(self.shader_sprite.program);
    REQUIRE(glGetError() == GL_NO_ERROR);

    glBindVertexArray(self.shader_sprite.vao);
    glBindBuffer(GL_ARRAY_BUFFER, self.shader_sprite.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STREAM_DRAW);
    REQUIRE(glGetError() == GL_NO_ERROR);

    glBindBuffer(GL_ARRAY_BUFFER, self.shader_sprite.vbo_instancing);
    glBufferData(GL_ARRAY_BUFFER, sizeof(SpriteInstance) * count, instances, GL_STREAM_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    REQUIRE(glGetError() == GL_NO_ERROR);

    glBindTexture(GL_TEXTURE_2D_ARRAY, (GLuint)spr->data_gl);
    REQUIRE(glGetError() == GL_NO_ERROR);

    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, count);
    REQUIRE(glGetError() == GL_NO_ERROR);

    glBindVertexArray(0);
}

ErrCode opengl_aquire_context()
{
    glfwMakeContextCurrent(self.glfw_window);
    return ErrCode::SUCCESS;
}

ErrCode opengl_release_context()
{
    glfwMakeContextCurrent(nullptr);
    return ErrCode::SUCCESS;
}

} // namespace tpx

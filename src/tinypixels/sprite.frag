#version 330 core
in vec2 frag_text_coord;
flat in int frag_subimage;
uniform sampler2DArray image_texture;

out vec4 FragColor;

void main()
{
    vec4 tex_color = texture(image_texture, vec3(frag_text_coord, frag_subimage));
    FragColor = tex_color;
}

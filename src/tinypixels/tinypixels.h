#pragma once

#include <cstdint>

namespace tpx
{
struct Window;

const int NO_MAX_FPS = 0;

enum class RenderType
{
    ERROR = -1,
    AUTO = 0,
    OPENGL = 1,
    VULKAN = 2
};

enum class ErrCode
{
    SUCCESS = 0,
    ERROR_UNKNOWN = 1,
    ERROR_NOT_SUPPORTED = 2,
    ERROR_INVALID_OPERATION = 3,
    ERROR_INVALID_ARGUMENT = 4
};

using DebugCallback = void(const char* msg, void* user_data);

class Color
{
public:
    Color() : rgba{0, 0, 0, 0xff}
    {
    }

    Color(int r, int g, int b, int a) : rgba{uint8_t(r), uint8_t(g), uint8_t(b), uint8_t(a)}
    {
    }

    Color(int r, int g, int b) : rgba{uint8_t(r), uint8_t(g), uint8_t(b), uint8_t(0xff)}
    {
    }

    Color(int hex)
        : rgba{uint8_t((hex & 0xff000000) >> 24), uint8_t((hex & 0x00ff0000) >> 16), uint8_t((hex & 0x0000ff00) >> 8), uint8_t(hex & 0x000000ff)}
    {
    }

    Color(uint8_t (&c)[4]) : rgba{c[0], c[1], c[2], c[3]}
    {
    }

    Color(uint8_t (&c)[3]) : rgba{c[0], c[1], c[2], 0xff}
    {
    }

    Color(int (&c)[4]) : rgba{uint8_t(c[0]), uint8_t(c[1]), uint8_t(c[2]), uint8_t(c[3])}
    {
    }

    Color(int (&c)[3]) : rgba{uint8_t(c[0]), uint8_t(c[1]), uint8_t(c[2]), 0xff}
    {
    }

    Color& operator=(const Color& c) = default;

    Color& operator=(int hex) noexcept
    {
        *this = Color(hex);
        return *this;
    }

    Color& operator=(uint8_t (&c)[4])
    {
        *this = Color(c);
        return *this;
    }

    Color& operator=(uint8_t (&c)[3])
    {
        *this = Color(c);
        return *this;
    }

    uint8_t operator[](int n) const
    {
        return rgba[n];
    }

    uint8_t& operator[](int n)
    {
        return rgba[n];
    }

    uint8_t rgba[4];
};

struct Camera
{
    int x;      // readonly, need to send to GPU
    int y;      // readonly, need to send to GPU
    int width;  // readonly
    int height; // readonly
    Color clear_color = DEFAULT_CLEAR_COLOR;
    inline static const Color DEFAULT_CLEAR_COLOR = Color(0, 0, 0x20);
};

inline Camera camera{};

struct Time
{
    int64_t frame_timestamp; // in us
    int64_t frame_delta;     // in us
    int64_t frame_no;
    int64_t update_period_target; // in us
};

inline Time time{};

struct SpriteResource
{
    int width;
    int height;
    int pivot_x;
    int pivot_y;
    int no_subimages;
    const uint32_t* ram_data;
    union {
        uint32_t data_gl;
        void* data_raw;
    };
};
#pragma pack(push, 1)
struct SpriteInstance
{
    int x;
    int y;
    int subimage;
};
#pragma pack(pop)

enum Key
{
    KEY_SPACE = 32,
    KEY_APOSTROPHE = 39,
    KEY_COMMA = 44,
    KEY_MINUS = 45,
    KEY_PERIOD = 46,
    KEY_SLASH = 47,
    KEY_0 = 48,
    KEY_1 = 49,
    KEY_2 = 50,
    KEY_3 = 51,
    KEY_4 = 52,
    KEY_5 = 53,
    KEY_6 = 54,
    KEY_7 = 55,
    KEY_8 = 56,
    KEY_9 = 57,
    KEY_SEMICOLON = 59,
    KEY_EQUAL = 61,
    KEY_A = 65,
    KEY_B = 66,
    KEY_C = 67,
    KEY_D = 68,
    KEY_E = 69,
    KEY_F = 70,
    KEY_G = 71,
    KEY_H = 72,
    KEY_I = 73,
    KEY_J = 74,
    KEY_K = 75,
    KEY_L = 76,
    KEY_M = 77,
    KEY_N = 78,
    KEY_O = 79,
    KEY_P = 80,
    KEY_Q = 81,
    KEY_R = 82,
    KEY_S = 83,
    KEY_T = 84,
    KEY_U = 85,
    KEY_V = 86,
    KEY_W = 87,
    KEY_X = 88,
    KEY_Y = 89,
    KEY_Z = 90,
    KEY_LEFT_BRACKET = 91,
    KEY_BACKSLASH = 92,
    KEY_RIGHT_BRACKET = 93,
    KEY_GRAVE_ACCENT = 96,
    KEY_WORLD_1 = 161,
    KEY_WORLD_2 = 162,
    KEY_ESCAPE = 256,
    KEY_ENTER = 257,
    KEY_TAB = 258,
    KEY_BACKSPACE = 259,
    KEY_INSERT = 260,
    KEY_DELETE = 261,
    KEY_RIGHT = 262,
    KEY_LEFT = 263,
    KEY_DOWN = 264,
    KEY_UP = 265,
    KEY_PAGE_UP = 266,
    KEY_PAGE_DOWN = 267,
    KEY_HOME = 268,
    KEY_END = 269,
    KEY_CAPS_LOCK = 280,
    KEY_SCROLL_LOCK = 281,
    KEY_NUM_LOCK = 282,
    KEY_PRINT_SCREEN = 283,
    KEY_PAUSE = 284,
    KEY_F1 = 290,
    KEY_F2 = 291,
    KEY_F3 = 292,
    KEY_F4 = 293,
    KEY_F5 = 294,
    KEY_F6 = 295,
    KEY_F7 = 296,
    KEY_F8 = 297,
    KEY_F9 = 298,
    KEY_F10 = 299,
    KEY_F11 = 300,
    KEY_F12 = 301,
    KEY_F13 = 302,
    KEY_F14 = 303,
    KEY_F15 = 304,
    KEY_F16 = 305,
    KEY_F17 = 306,
    KEY_F18 = 307,
    KEY_F19 = 308,
    KEY_F20 = 309,
    KEY_F21 = 310,
    KEY_F22 = 311,
    KEY_F23 = 312,
    KEY_F24 = 313,
    KEY_F25 = 314,
    KEY_KP_0 = 320,
    KEY_KP_1 = 321,
    KEY_KP_2 = 322,
    KEY_KP_3 = 323,
    KEY_KP_4 = 324,
    KEY_KP_5 = 325,
    KEY_KP_6 = 326,
    KEY_KP_7 = 327,
    KEY_KP_8 = 328,
    KEY_KP_9 = 329,
    KEY_KP_DECIMAL = 330,
    KEY_KP_DIVIDE = 331,
    KEY_KP_MULTIPLY = 332,
    KEY_KP_SUBTRACT = 333,
    KEY_KP_ADD = 334,
    KEY_KP_ENTER = 335,
    KEY_KP_EQUAL = 336,
    KEY_LEFT_SHIFT = 340,
    KEY_LEFT_CONTROL = 341,
    KEY_LEFT_ALT = 342,
    KEY_LEFT_SUPER = 343,
    KEY_RIGHT_SHIFT = 344,
    KEY_RIGHT_CONTROL = 345,
    KEY_RIGHT_ALT = 346,
    KEY_RIGHT_SUPER = 347,
    KEY_MENU = 348,
};

struct Input
{
    bool is_down[348];
    bool is_released[348];
    bool is_pressed[348];
    bool is_close_window;
    int64_t timestamp;
};

inline Input input;

ErrCode open_window(const char* title,
                    int width,
                    int height,
                    bool fullscreen = false,
                    bool vsync = true,
                    int max_fps = NO_MAX_FPS,
                    RenderType render_type = RenderType::AUTO);
ErrCode input_update(Input* input);
ErrCode start_debug(DebugCallback* callback, void* user_data);
ErrCode send_debug(const char* msg);
ErrCode close_window();
ErrCode window_close_requested(bool* close_requested);
void next_frame();
void camera_move(int x, int y);
ErrCode time_now(int64_t* total);

inline void (*draw_clear)(void);
inline void (*draw_line)(int start_x, int start_y, int stop_x, int stop_y, Color start_color, Color stop_color);
inline void (*draw_sprite)(const SpriteResource* img, int x, int y, int subimage);
inline void (*draw_sprites)(const SpriteResource* img, const SpriteInstance* pkg, int count);
//inline void (*read_pixels)(int x, int y, int width, int height, Color* colors); //todo(markusl): todo
inline ErrCode (*resource_load_sprite)(SpriteResource* sprite_resource);
inline ErrCode (*resource_unload_sprite)(SpriteResource* sprite_resource);
inline ErrCode (*aquire_context)();
inline ErrCode (*release_context)();
ErrCode start_gui_loop();
ErrCode stop_gui_loop();

} // namespace tpx

#pragma once

#include "tinypixels.h"

struct GLFWwindow;

namespace tpx
{
ErrCode opengl_init(GLFWwindow** glfw_window, const char* title, int width, int height, bool fullscreen, bool vsync);
ErrCode opengl_destroy();
ErrCode opengl_start_debug(DebugCallback callback, void* userData);
ErrCode opengl_next_frame();
ErrCode opengl_update_camera(int x, int y, int width, int height);
} // namespace tpx

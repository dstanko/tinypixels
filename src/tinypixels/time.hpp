#include <cstdint>
#include <chrono>

namespace utils
{
// ticktock in microseconds
int64_t ticktock()
{
    static auto s_lastTimeStamp = std::chrono::high_resolution_clock::now();
    auto t2 = std::chrono::high_resolution_clock::now();
    int64_t elapsed = int64_t(std::chrono::duration_cast<std::chrono::microseconds>(t2 - s_lastTimeStamp).count());
    s_lastTimeStamp = t2;
    return elapsed;
}
} // namespace utils

#version 330 core
layout (location = 0) in vec2 quad_pos;
layout (location = 1) in vec2 tex_coord;
layout (location = 2) in vec2 pos;
layout (location = 3) in int subimage;
uniform mat4 camera;

out vec2 frag_text_coord;
flat out int frag_subimage;

void main()
{
    gl_Position = camera * vec4(quad_pos + pos, 0.0, 1.0);
    frag_text_coord = tex_coord;
    frag_subimage = subimage;
}

#include <cstdint>

namespace
{
// TODO: Make this global
static uint8_t s_staticStack[64 * 1024]; // One page, maybe we should align it?
static int64_t s_stackCounter = 0;
} // namespace

namespace utils
{
template <typename T>
class MemBlock
{
public:
    explicit MemBlock(int64_t elements) : elements(elements), data((T*)(s_staticStack + s_stackCounter))
    {
        s_stackCounter += sizeof(T) * elements;
    }
    ~MemBlock()
    {
        s_stackCounter -= sizeof(T) * elements;
    }
    MemBlock(MemBlock&) = delete;
    MemBlock(MemBlock&&) = delete;
    MemBlock& operator=(MemBlock&) = delete;
    MemBlock& operator=(MemBlock&&) = delete;

    T* data;
    int64_t elements;
};

// Stack on stack
template <typename T, int64_t capacity>
class MemStack
{
public:
    MemStack() : elements(0), data()
    {
    }
    T* reserve(int32_t size)
    {
        T* ptr = data + elements;
        elements += size;
        return ptr;
    }
    MemStack(MemStack&) = delete;
    MemStack(MemStack&&) = delete;
    MemStack& operator=(MemStack&) = delete;
    MemStack& operator=(MemStack&&) = delete;

    T data[capacity];
    int64_t elements;
};
} // namespace utils
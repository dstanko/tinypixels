#include "tinypixels.h"

#include "memory.hpp"

#ifdef USE_VULKAN
#include <vulkan/vulkan.h> // Before glfw3
#endif
#include <GLFW/glfw3.h>
#include <vector> // memset

#if _MSC_VER
#pragma warning(disable : 26812)
#endif

using utils::MemBlock;
using utils::MemStack;

namespace tpx
{
#ifdef USE_VULKAN
struct RendererVulkan
{
    VkInstance vkinstance;
    VkDevice vkdevice;
    VkQueue graphics_queue;
    VkQueue surface_queue;
    VkSurfaceKHR surface;
} g_renderer_vulkan;

#ifdef USE_VULKAN_VALIDATION_LAYER
VkDebugUtilsMessengerEXT g_vulkanDebugMessengerCtx = nullptr;

VKAPI_ATTR VkBool32 VKAPI_CALL vulkan_debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                                     VkDebugUtilsMessageTypeFlagsEXT messageType,
                                                     const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                                     void* pUserData)
{
    send_debug(pCallbackData->pMessage);
    return VK_FALSE; // Should abort
}
#endif
#endif
ErrCode init_vulkan(GLFWwindow** glfw_window, const char* title, int width, int height, bool fullscreen, bool vsync)
{
#ifndef USE_VULKAN
    return ErrCode::ERROR_NOT_SUPPORTED;
#elif

    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    *glfw_window = glfwCreateWindow(width, height, title, nullptr, nullptr);

    if (*glfw_window == nullptr)
    {
        return ErrCode::ERROR_UNKNOWN;
    }

    VkApplicationInfo app_info = {};
    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pApplicationName = title;
    app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    app_info.pEngineName = "YAGE";
    app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    app_info.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instance_create_info = {};
    instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instance_create_info.pApplicationInfo = &app_info;

    uint32_t glfw_extension_count = 0;
    const char** glfw_extensions;
    glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);
    MemStack<const char*, 10> required_extensions;
    memcpy(required_extensions.reserve(glfw_extension_count), glfw_extensions, glfw_extension_count * sizeof(const char*));

#ifdef USE_VULKAN_VALIDATION_LAYER
    // Enable validation layers (LunarG Vulkan SDK)

    uint32_t extension_count = 0;
    vkEnumerateInstanceLayerProperties(&extension_count, nullptr);
    MemBlock<VkLayerProperties> layers(extension_count);
    vkEnumerateInstanceLayerProperties(&extension_count, layers.data);

    const char* validation_layer_name = "VK_LAYER_LUNARG_standard_validation";
    bool has_validation_layer_support = false;
    for (int i = 0; i < (int)layers.elements; ++i)
    {
        if (std::strcmp(layers.data[i].layerName, validation_layer_name) == 0)
        {
            has_validation_layer_support = true;
            break;
        }
    }
    if (has_validation_layer_support)
    {
        instance_create_info.enabledLayerCount = 1;
        instance_create_info.ppEnabledLayerNames = &validation_layer_name;

        *required_extensions.reserve(1) = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
    }
    else
    {
        return ErrCode::ERROR_NOT_SUPPORTED;
    }
#endif

    // Create instance
    instance_create_info.enabledExtensionCount = (uint32_t)required_extensions.elements;
    instance_create_info.ppEnabledExtensionNames = required_extensions.data;

    VkResult result = vkCreateInstance(&instance_create_info, nullptr, &g_renderer_vulkan.vkinstance);

    if (result != VK_SUCCESS)
        return ErrCode::ERROR_NOT_SUPPORTED;

    if (glfwCreateWindowSurface(g_renderer_vulkan.vkinstance, *glfw_window, nullptr, &g_renderer_vulkan.surface) != VK_SUCCESS)
        return ErrCode::ERROR_NOT_SUPPORTED;

    // Find a GPU
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(g_renderer_vulkan.vkinstance, &deviceCount, nullptr);
    if (deviceCount == 0)
        return ErrCode::ERROR_NOT_SUPPORTED;
    MemBlock<VkPhysicalDevice> devices(deviceCount);
    MemBlock<int32_t> deviceScore(deviceCount);
    memset(deviceScore.data, -1, deviceScore.elements * sizeof(int32_t));

    vkEnumeratePhysicalDevices(g_renderer_vulkan.vkinstance, &deviceCount, devices.data);

    for (uint32_t i = 0; i < deviceCount; ++i)
    {
        // Check required features
        VkPhysicalDeviceFeatures deviceFeatures;
        vkGetPhysicalDeviceFeatures(devices.data[i], &deviceFeatures);
        if (!deviceFeatures.geometryShader)
            continue;

        // Check required extensions
        uint32_t extCount = 0;
        vkEnumerateDeviceExtensionProperties(devices.data[i], nullptr, &extCount, nullptr);
        MemBlock<VkExtensionProperties> ext_props(extCount);
        vkEnumerateDeviceExtensionProperties(devices.data[i], nullptr, &extCount, ext_props.data);
        bool hasSwapChain = false;
        for (int j = 0; j < (int)extCount; ++j)
        {
            if (strcmp(ext_props.data[j].extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME) == 0)
                hasSwapChain = true;
        }
        if (!hasSwapChain)
            continue;

        bool hasValidFormat = false;

        VkSurfaceCapabilitiesKHR capabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(devices.data[i], g_renderer_vulkan.surface, &capabilities);

        uint32_t formatCount;
        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(devices.data[i], g_renderer_vulkan.surface, &formatCount, nullptr);
        vkGetPhysicalDeviceSurfacePresentModesKHR(devices.data[i], g_renderer_vulkan.surface, &presentModeCount, nullptr);

        MemBlock<VkSurfaceFormatKHR> surfaceFormats(formatCount);
        MemBlock<VkPresentModeKHR> presentModes(presentModeCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(devices.data[i], g_renderer_vulkan.surface, &formatCount, surfaceFormats.data);
        vkGetPhysicalDeviceSurfacePresentModesKHR(devices.data[i], g_renderer_vulkan.surface, &presentModeCount, presentModes.data);

        for (int i = 0; i < (int)formatCount; ++i)
        {
            if (surfaceFormats.data[i].format == VK_FORMAT_B8G8R8A8_UNORM && surfaceFormats.data[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            {
                hasValidFormat = true;
                break;
            }
        }
        if (!hasValidFormat)
        {
            return ErrCode::ERROR_NOT_SUPPORTED;
        }

        bool hasValidPrentMode = false;
        VkPresentModeKHR requestedPresentMode;
        if (vsync)
        {
            // Required to be supplied by driver
            requestedPresentMode = VK_PRESENT_MODE_FIFO_KHR;
            hasValidPrentMode = true;
        }
        else
        {
            // Do something nice here
            requestedPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;

            for (int i = 0; i < (int)presentModeCount; ++i)
            {
                if (presentModes.data[i] == requestedPresentMode)
                {
                    hasValidPrentMode = true;
                }
            }
        }

        if (!hasValidPrentMode)
        {
            return ErrCode::ERROR_NOT_SUPPORTED;
        }

        // So far so good
        deviceScore.data[i] = 0;

        // Try to calculate score based on properties
        VkPhysicalDeviceProperties deviceProperties;
        vkGetPhysicalDeviceProperties(devices.data[i], &deviceProperties);

        if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            deviceScore.data[i] += 40;
    }

    // Use best score
    int maxPoints = -1;
    int bestDeviceIndex = -1;
    for (int i = 0; i < (int32_t)deviceCount; ++i)
    {
        if (deviceScore.data[i] > maxPoints)
        {
            maxPoints = deviceScore.data[i];
            bestDeviceIndex = i;
        }
    }

    if (bestDeviceIndex >= 0)
    {
        physicalDevice = devices.data[bestDeviceIndex];
    }
    else
    {
        return ErrCode::ERROR_NOT_SUPPORTED;
    }

    // Now set up the queues
    uint32_t queueFamilyPropertiesCount;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertiesCount, nullptr);
    MemBlock<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyPropertiesCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertiesCount, queueFamilyProperties.data);

    uint32_t graphicsQueueIndex = 0;
    VkBool32 hasGraphicsQueueIndex = false;

    uint32_t surfaceQueueIndex = 0;
    VkBool32 hasSurfaceQueueIndex = false;

    // Find queues
    VkDeviceQueueCreateInfo queueCreateInfos[2] = {}; // We need to default init each used element anyway
    uint32_t numberOfIndices = 0;
    float queuePriority = 1.0f;

    for (uint32_t i = 0; i < queueFamilyPropertiesCount; ++i)
    {
        bool useThisIndex = false;

        if (!hasSurfaceQueueIndex)
        {
            vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, g_renderer_vulkan.surface, &hasSurfaceQueueIndex);
            if (queueFamilyProperties.data[i].queueCount > 0 && hasSurfaceQueueIndex)
            {
                surfaceQueueIndex = i;
                useThisIndex = true;
            }
        }

        if (!hasGraphicsQueueIndex)
        {
            if (queueFamilyProperties.data[i].queueCount > 0 && queueFamilyProperties.data[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
            {
                graphicsQueueIndex = i;
                hasGraphicsQueueIndex = true;
                useThisIndex = true;
            }
        }

        if (useThisIndex)
        {
            queueCreateInfos[numberOfIndices].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfos[numberOfIndices].queueFamilyIndex = i;
            queueCreateInfos[numberOfIndices].queueCount = 1;
            queueCreateInfos[numberOfIndices].pQueuePriorities = &queuePriority;

            numberOfIndices++;
        }

        if (hasSurfaceQueueIndex && hasGraphicsQueueIndex)
            break;
    }

    if (!hasGraphicsQueueIndex || !hasSurfaceQueueIndex)
        return ErrCode::ERROR_NOT_SUPPORTED;

    // Create logical device
    VkDeviceCreateInfo deviceCreateInfo = {};
    const char* deviceFeatures[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pQueueCreateInfos = queueCreateInfos;
    deviceCreateInfo.queueCreateInfoCount = numberOfIndices;
    deviceCreateInfo.ppEnabledExtensionNames = deviceFeatures;
    deviceCreateInfo.enabledExtensionCount = 1;

#ifdef USE_VULKAN_VALIDATION_LAYER
    if (has_validation_layer_support)
    {
        deviceCreateInfo.enabledLayerCount = 1;
        deviceCreateInfo.ppEnabledLayerNames = &validation_layer_name;
    }
#endif

    if (vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &g_renderer_vulkan.vkdevice) != VK_SUCCESS)
        return ErrCode::ERROR_NOT_SUPPORTED;

    vkGetDeviceQueue(g_renderer_vulkan.vkdevice, graphicsQueueIndex, 0, &g_renderer_vulkan.graphics_queue);
    vkGetDeviceQueue(g_renderer_vulkan.vkdevice, surfaceQueueIndex, 0, &g_renderer_vulkan.surface_queue);

    return ErrCode::SUCCESS;
#endif
}
ErrCode destroy_vulkan(GLFWwindow** glfw_window)
{
#ifndef USE_VULKAN
    return ErrCode::ERROR_NOT_SUPPORTED;
#else
#ifdef USE_VULKAN_VALIDATION_LAYER
    vkDestroyDevice(g_renderer_vulkan.vkdevice, nullptr);
    if (g_vulkanDebugMessengerCtx != 0)
    {
        auto p = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(g_renderer_vulkan.vkinstance, "vkDestroyDebugUtilsMessengerEXT");
        p((VkInstance)g_renderer_vulkan.vkinstance, g_vulkanDebugMessengerCtx, nullptr);
        g_vulkanDebugMessengerCtx = 0;
    }
#endif

    if (g_renderer_vulkan.vkinstance != nullptr)
    {
        vkDestroySurfaceKHR(g_renderer_vulkan.vkinstance, g_renderer_vulkan.surface, nullptr);
        vkDestroyInstance(g_renderer_vulkan.vkinstance, nullptr);
        g_renderer_vulkan.vkinstance = nullptr;
    }

    if (*glfw_window != nullptr)
    {
        glfwDestroyWindow(*glfw_window);
        *glfw_window = nullptr;
    }

    glfwTerminate();
    return ErrCode::SUCCESS;
#endif
}

ErrCode start_debug_vulkan(DebugCallback callback, void* userData)
{
#ifdef USE_VULKAN_VALIDATION_LAYER
    VkDebugUtilsMessengerCreateInfoEXT debug_create_info = {};
    debug_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debug_create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debug_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                    VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    debug_create_info.pfnUserCallback = vulkan_debug_callback;
    debug_create_info.pUserData = userData;

    auto p = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(g_renderer_vulkan.vkinstance, "vkCreateDebugUtilsMessengerEXT");
    p(g_renderer_vulkan.vkinstance, &debug_create_info, nullptr, &g_vulkanDebugMessengerCtx);
#endif
    return ErrCode::SUCCESS;
}
} // namespace tpx

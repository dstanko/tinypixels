#pragma once

#include "tinypixels.h"

namespace tpx
{
ErrCode init_vulkan(GLFWwindow** glfw_window, const char* title, int width, int height, bool fullscreen, bool vsync);
ErrCode destroy_vulkan(GLFWwindow** glfw_window);
ErrCode start_debug_vulkan(DebugCallback callback, void* userData);
} // namespace tinypixels

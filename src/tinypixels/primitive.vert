#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec4 aColor;
uniform mat4 camera;

out vec4 ourColor;

void main()
{
    gl_Position = camera * vec4(aPos.x + 0.5, aPos.y + 0.5, 0.0, 1.0);
    ourColor = aColor;
}

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>

namespace fs = std::filesystem;

bool ends_with(std::string const& src, std::string const& ending)
{
    if (src.length() >= ending.length())
    {
        return (0 == src.compare(src.length() - ending.length(), ending.length(), ending));
    }
    else
    {
        return false;
    }
}

int main(const int argc, const char* argv[])
{
    if (argc != 3)
    {
        std::cout << "argument error" << std::endl;
        return -1;
    }

    const char* input_dir = argv[1];
    const char* output_file = argv[2];

    std::ofstream o(output_file);
    o << "#pragma once\n";

    std::cout << "reading from: " << input_dir << std::endl;

    for (const fs::directory_entry& entry : fs::directory_iterator(input_dir))
    {
        if (entry.is_regular_file())
        {
            if (entry.path().extension().compare(".frag") == 0)
            {
                std::ifstream t(entry.path());
                std::string txt((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

                std::cout << "found fragment shader: " << entry.path() << std::endl;
                std::string name = entry.path().stem().string();
                o << "\ninline const char* shader_fragment_" << name << "_src = R\"\"\"(\n";
                o << txt;
                o << "\n)\"\"\";\n";
            }
            else if (entry.path().extension().compare(".vert") == 0)
            {
                std::ifstream t(entry.path());
                std::string txt((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

                std::cout << "found vertex shader: " << entry.path() << std::endl;
                std::string name = entry.path().stem().string();
                o << "\ninline const char* shader_vertex_" << name << "_src = R\"\"\"(\n";
                o << txt;
                o << "\n)\"\"\";\n";
            }
        }
    }

    return 0;
}

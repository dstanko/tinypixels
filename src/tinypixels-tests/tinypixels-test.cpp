#include <tinypixels.h>

#include <resources.h>

#include <doctest.h>
#include <iostream>

#if _MSC_VER
#pragma warning(disable : 6319)
#endif

namespace tpx
{
TEST_CASE("init game - AUTO")
{
    // Given
    bool vsync = true;
    bool fullscreen = false;
    int width = 800;
    int height = 600;

    // When
    ErrCode start_code = open_window("TinyPixels Test", width, height, fullscreen, vsync);

    // Then
    CHECK_EQ(start_code, ErrCode::SUCCESS);

    // Cleanup
    close_window();
}

TEST_CASE("init game - VULKAN")
{
    // Given
    bool vsync = true;
    bool fullscreen = false;
    int width = 800;
    int height = 600;

    // When
    ErrCode start_code = open_window("TinyPixels Test", width, height, fullscreen, vsync, NO_MAX_FPS, RenderType::VULKAN);

// Then
#ifdef USE_VULKAN
    CHECK(start_code == ErrCode::SUCCESS);
#else
    CHECK(start_code == ErrCode::ERROR_NOT_SUPPORTED);
#endif

    // Cleanup
    close_window();
}

TEST_CASE("init game - OpenGL")
{
    // Given
    bool vsync = true;
    bool fullscreen = false;
    int width = 800;
    int height = 600;

    // When
    ErrCode start_code = open_window("TinyPixels Test", width, height, fullscreen, vsync, NO_MAX_FPS, RenderType::OPENGL);

    // Then
    CHECK_EQ(start_code, ErrCode::SUCCESS);

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("game free")
{
    // Given
    open_window("TinyPixels Test", 800, 600);

    // When
    ErrCode free_code = close_window();

    // Then
    CHECK(free_code == ErrCode::SUCCESS);
}

TEST_CASE("game input")
{
    // Given
    open_window("TinyPixels Test", 800, 600);

    // When
    bool any_error = false;
    for (int i = 0; i < 50000; ++i)
    {
        tpx::Input input;
        ErrCode input_status = input_update(&input);
        if (input_status != ErrCode::SUCCESS)
        {
            any_error = true;
        }
    }

    // Then
    CHECK_EQ(any_error, false);

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("frame steps")
{
    // Given
    open_window("TinyPixels Test", 800, 600);

    // When
    bool any_error = false;
    for (int i = 0; i < 30; ++i)
    {
        next_frame();
    }

    // Then
    // any internal checks in next_frame should not trigger

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("draw line")
{
    // Given
    open_window("TinyPixels Test", 800, 600);

    // When

    Color stop_color = 0x0000ffff;
    bool any_error = false;
    for (int i = 0; i < 51; ++i)
    {
        Color start_color = {i * 5, i * 5, i * 5};
        draw_line(32, 32, 800, 600, start_color, stop_color);
        draw_line(32, 32, 64, 32, start_color, stop_color);
        draw_line(32, 32, 32, 64, start_color, stop_color);
        next_frame();
    }

    // Then
    // any internal checks in draw_line should not trigger

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("start debug")
{
    // Given
    open_window("TinyPixels Test", 800, 600);

    // When
    ErrCode s = start_debug([](const char* msg, void* user_param) -> void { std::cout << "message: " << msg << std::endl; }, nullptr);

    // Then
    CHECK(s == ErrCode::SUCCESS);

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("send debug")
{
    // Given
    open_window("TinyPixels Test", 800, 600);
    bool called = false;
    auto& L = [](const char* msg, void* user_param) -> void { *((bool*)user_param) = true; };
    start_debug(L, &called);

    // When
    ErrCode s = send_debug("test");

    // Then
    CHECK(s == ErrCode::SUCCESS);
    CHECK(called == true);

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

#if TEST_DEBUG_OPENGL_MSG
// todo(markusl): driver dependent
TEST_CASE("send debug OpenGL")
{
    // Given
    REQUIRE(open_window("TinyPixels Test", 800, 600, false, true, NO_MAX_FPS, RenderType::OPENGL) == ErrCode::SUCCESS);
    bool called = false;
    auto& L = [](const char* msg, void* user_param) -> void { *((bool*)user_param) = true; };
    REQUIRE(start_debug(L, &called) == ErrCode::SUCCESS);
    Color color = {255, 0, 0, 255};

    // When
    draw_line(32, 32, 800, 600, color, color);

    // Then
    CHECK(called == true);

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}
#endif

TEST_CASE("sprite resource load and unload")
{
    // Given
    REQUIRE(open_window("TinyPixels Test", 800, 600) == ErrCode::SUCCESS);

    // When
    ErrCode load_sts = resource_load_sprite(&sprite_test_single);
    ErrCode unload_sts = resource_unload_sprite(&sprite_test_single);

    // Then
    CHECK(load_sts == ErrCode::SUCCESS);
    CHECK(unload_sts == ErrCode::SUCCESS);

    // Cleanup
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("draw sprite")
{
    // Given
    open_window("TinyPixels Test", 800, 600);

    ErrCode load_sts = resource_load_sprite(&sprite_test_multi);

    camera.clear_color = 0x000000ff;

    // When
    bool any_error = false;
    for (int i = 0; i < 128; ++i)
    {
        camera.clear_color[0] += 1;
        camera.clear_color[1] += 2;
        camera.clear_color[2] += 1;
        draw_sprite(&sprite_test_multi, 200, 150, (i / 20) % 3);
        next_frame();
    }

    // Then
    // any internal checks in draw_sprite should not trigger

    // Cleanup
    REQUIRE(resource_unload_sprite(&sprite_test_multi) == ErrCode::SUCCESS);
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("camera move +x")
{
    // Given
    open_window("TinyPixels Test", 800, 600);
    ErrCode load_sts = resource_load_sprite(&sprite_test_single);

    // When
    bool any_error = false;
    for (int i = 0; i < 100; ++i)
    {
        draw_sprite(&sprite_test_single, 400, 300, 0);
        camera_move(i, 0);
        next_frame();
    }

    // Then
    // any internal checks in camera_move should not trigger

    // Cleanup
    REQUIRE(resource_unload_sprite(&sprite_test_single) == ErrCode::SUCCESS);
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("camera move +y")
{
    // Given
    open_window("TinyPixels Test", 800, 600);
    ErrCode load_sts = resource_load_sprite(&sprite_test_single);

    // When
    bool any_error = false;
    for (int i = 0; i < 100; ++i)
    {
        draw_sprite(&sprite_test_single, 400, 300, 0);
        camera_move(0, i);
        next_frame();
    }

    // Then
    // any internal checks in camera_move should not trigger

    // Cleanup
    REQUIRE(resource_unload_sprite(&sprite_test_single) == ErrCode::SUCCESS);
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("text")
{
    // Given
    open_window("TinyPixels Test", 800, 600);
    ErrCode load_sts = resource_load_sprite(&sprite_font_roboto_mono);

    // When
    bool any_error = false;
    for (int i = 0; i < 256; ++i)
    {
        camera.clear_color[0] += 1;
        camera.clear_color[1] += 2;
        camera.clear_color[2] += 1;
        draw_sprite(&sprite_font_roboto_mono, 200, 150, i);

        constexpr char32_t msg[] = U"Hello World!";
        constexpr size_t LENGTH = std::char_traits<char32_t>::length(msg);

        SpriteInstance hw[LENGTH];

        for (int n = 0; n < LENGTH; ++n)
        {
            hw[n].x = 400 + n * 16;
            hw[n].y = 200;
            hw[n].subimage = msg[n];
        }
        draw_sprites(&sprite_font_roboto_mono, hw, LENGTH);
        next_frame();
    }

    // Then
    // any internal checks in camera_move should not trigger

    // Cleanup
    REQUIRE(resource_unload_sprite(&sprite_font_roboto_mono) == ErrCode::SUCCESS);
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

TEST_CASE("draw images")
{
    // Given
    REQUIRE(open_window("TinyPixels Test", 800, 600) == ErrCode::SUCCESS);
    REQUIRE(resource_load_sprite(&sprite_test_single) == ErrCode::SUCCESS);

    const int NO_OF_PACKAGES = 5;
    SpriteInstance packages[NO_OF_PACKAGES];
    for (int i = 0; i < NO_OF_PACKAGES; ++i)
    {
        packages[i].x = 10 + 100 * i;
        packages[i].y = 10 + 100 * i;
    }

    // When
    for (int i = 0; i < 128; ++i)
    {
        draw_sprites(&sprite_test_single, packages, NO_OF_PACKAGES);
        next_frame();
    }

    // Then
    // any internal checks in draw_images should not trigger

    // Cleanup
    REQUIRE(resource_unload_sprite(&sprite_test_single) == ErrCode::SUCCESS);
    REQUIRE(close_window() == ErrCode::SUCCESS);
}

} // namespace tpx

int main(int argc, char** argv)
{
    doctest::Context context;
    return context.run();
}
